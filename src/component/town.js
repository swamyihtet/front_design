import React from 'react';
import MaterialTable from 'material-table';

function Town_Table() {
  const { useState } = React;

  const [columns, setColumns] = useState([
    { title: 'Towns ID', field: 'towns_id', align:'center'},
    { title: 'Towns Code', field: 'towns_code' ,align:'center'},
    { title: 'Towns Name', field: 'towns_name',align:'center'},
    { title: 'State Region',field: 'state_region' ,align:'center'},
    { title: 'Township',field: 'township' ,align:'center'},
    { title: 'District',field: 'district' ,align:'center'},
    { title: 'Remark',field: 'remark' ,align:'center'},
  ]);

  const [data, setData] = useState([
    { towns_id: '1', towns_code: '04201', towns_name: 'Pakokku', state_region: 'Magway Region' ,township: 'Pakokku Township', district: 'Pakokku District', remark:''},
    { towns_id: '2', towns_code: '05041', towns_name: 'Chanmyathazi', state_region: 'Mandalay Region' ,township: 'Chanmyatharzi Township', district: 'Mandalay District', remark:''},
    { towns_id: '3', towns_code: '15012', towns_name: 'Pyinmana', state_region: 'Naypyidaw Union Territory' ,township: 'Pyinmana Township', district: 'Dekkhina Kistrict', remark:''},
    { towns_id: '4', towns_code: '09011', towns_name: 'Loikaw', state_region: 'Kayah State' ,township: 'Loikaw Township', district: 'Loikaw District', remark:''},
    { towns_id: '5', towns_code: '06024', towns_name: 'Minewa', state_region: 'Shan State' ,township: 'Kalaw Township', district: 'Taunggyi District', remark:''},
    { towns_id: '6', towns_code: '11162', towns_name: 'Botahtaung', state_region: 'Yangon Region' ,township: 'Bohtataung Township', district: 'East Yangon District', remark:''},
    { towns_id: '7', towns_code: '01011', towns_name: 'Myintkyina', state_region: 'Kachin State' ,township: 'Myitkyina Township', district: 'Myitkyina District', remark:''},
    { towns_id: '8', towns_code: '13018', towns_name: 'Barkat', state_region: 'Kayin State' ,township: 'Pha-an Township', district: 'Pha-an District', remark:''},
    { towns_id: '9', towns_code: '12101', towns_name: 'Ye', state_region: 'Mon State' ,township: 'Ye Township', district: 'Mawlamyime  District', remark:''},
    { towns_id: '10', towns_code: '03011', towns_name: 'Hakha', state_region: 'Chin State' ,township: 'Hakha Township', district: 'Hakha District', remark:''},
  
  ]);
  return (
    <MaterialTable
      title="Town Preview"
      columns={columns}
      data={data}

      options={{
        paging:false,
        headerStyle:{fontWeight:'bold', fontSize:16, color:'#000', fontFamily:'Poppins'},
        rowStyle:{fontWeight:'normal', fontSize:16, color:'#505050', fontFamily:'Poppins'},
        actionsCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'},
        filterCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'},
        
      }}
      
      editable={{

        onRowAdd: newData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              setData([...data, newData]);
              resolve();
            }, 1000)
          }),

        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const dataUpdate = [...data];
              const index = oldData.tableData.id;
              dataUpdate[index] = newData;
              setData([...dataUpdate]);

              resolve();
            }, 1000)
          }),

        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const dataDelete = [...data];
              const index = oldData.tableData.id;
              dataDelete.splice(index, 1);
              setData([...dataDelete]);
              
              resolve()
            }, 1000)
          }),
      }}
    />
  )
}

export default Town_Table;