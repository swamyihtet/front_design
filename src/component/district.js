import React from 'react';
import MaterialTable from 'material-table';

function District_Table() {
  const { useState } = React;

  const [columns, setColumns] = useState([
    { title: 'District ID',   field: 'district_id', align:'center'},
    { title: 'District Code', field: 'district_code' ,align:'center'},
    { title: 'District Name', field: 'district_name',align:'center'},
    { title: 'State Region',  field: 'state_region' ,align:'center'},
    { title: 'Remark',  field: 'remark' ,align:'center'},
  ]);

  const [data, setData] = useState([
    { district_id: '1', district_code: '04', district_name: 'Pakokku District', state_region: 'Magway Region',remark: '' },
    { district_id: '2', district_code: '05', district_name: 'Mandalay District', state_region: 'Mandalay Region',remark: '' },
    { district_id: '3', district_code: '15', district_name: 'Dekkhina Kistrict', state_region: 'Naypyidaw Region' },
    { district_id: '4', district_code: '09', district_name: 'Loikaw District', state_region: 'Kayah State' },
    { district_id: '5', district_code: '06', district_name: 'Taunggyi District', state_region: 'Shan State' },
    { district_id: '6', district_code: '11', district_name: 'East Yangon District', state_region: 'Yangon Region' },
    { district_id: '7', district_code: '01', district_name: 'Myitkyina District', state_region: 'Kachin State' },
    { district_id: '8', district_code: '13', district_name: 'Pha-an District', state_region: 'Kayin State' },
    { district_id: '9', district_code: '12', district_name: 'Mawlamyime  District', state_region: 'Mon State' },
    { district_id: '10', district_code: '03', district_name: 'Hakha District', state_region: 'Chin State' },
    // { district_id: '11', district_code:5'119023', district_name: 'Naga Zone', state_region: 'Nothing to say' },
    // { district_id: '12', district_code: '119023', district_name: 'Bago District', state_region: 'Nothing to say' },
    // { district_id: '13', district_code: '119023', district_name: 'Taungoo District', state_region: 'Nothing to say' },
    // { district_id: '14', district_code: '119023', district_name: 'Pyay District', state_region: 'Nothing to say' },
    // { district_id: '15', district_code: '119023', district_name: 'Tharrawaddy District', state_region: 'Nothing to say' },
    // { district_id: '16', district_code: '119023', district_name: 'Gangaw District', state_region: 'Nothing to say' },
    // { district_id: '17', district_code: '119023', district_name: 'Magway District', state_region: 'Nothing to say' },
    // { district_id: '28', district_code: '119023', district_name: 'Minbu District', state_region: 'Nothing to say' },
    // { district_id: '29', district_code: '119023', district_name: 'Pakokku District', state_region: 'Nothing to say' },
    // { district_id: '20', district_code: '119023', district_name: 'Thayet District', state_region: 'Nothing to say' },
  ]);

  return (
    <MaterialTable
      title="District Preview"
      columns={columns}
      data={data}

      options={{
        paging:false,
        headerStyle:{fontWeight:'bold', fontSize:16, color:'#000', fontFamily:'Poppins'},
        rowStyle:{fontWeight:'normal', fontSize:16, color:'#505050', fontFamily:'Poppins'},
        actionsCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'},
        filterCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'},
        
      }}
      
      editable={{

        onRowAdd: newData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              setData([...data, newData]);
              resolve();
            }, 1000)
          }),

        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const dataUpdate = [...data];
              const index = oldData.tableData.id;
              dataUpdate[index] = newData;
              setData([...dataUpdate]);

              resolve();
            }, 1000)
          }),

        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const dataDelete = [...data];
              const index = oldData.tableData.id;
              dataDelete.splice(index, 1);
              setData([...dataDelete]);
              
              resolve()
            }, 1000)
          }),
      }}
    />
  )
}

export default District_Table;