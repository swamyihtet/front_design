import React from 'react';
import MaterialTable from 'material-table';

function State_Table() {
  const { useState } = React;

  const [columns, setColumns] = useState([
    { title: 'State ID', field: 'state_id', align:'center'},
    { title: 'State Code', field: 'state_code' ,align:'center'},
    { title: 'State Region', field: 'state_region',align:'center'},
    {title: 'Remark',field: 'remark' ,align:'center'},
  ]);

  const [data, setData] = useState([
    { state_id: 'State ID', state_code: 'MM-01', state_region: 'Sagaing', remark: 'Nothing to say' },
    { state_id: '2', state_code: 'MM-02', state_region: 'Bago', remark: 'Nothing to say' },
    { state_id: '3', state_code: 'MM-03', state_region: 'Magway', remark: 'Nothing to say' },
    { state_id: '4', state_code: 'MM-04', state_region: 'Mandalay', remark: 'Nothing to say' },
    { state_id: '5', state_code: 'MM-05', state_region: 'Tanintharyi', remark: 'Nothing to say' },
    { state_id: '6', state_code: 'MM-06', state_region: 'Yangon', remark: 'Nothing to say' },
    { state_id: '7', state_code: 'MM-07', state_region: 'Ayeyarwady', remark: 'Nothing to say' },
    { state_id: '8', state_code: 'MM-11', state_region: 'Kachin', remark: 'Nothing to say' },
    { state_id: '9', state_code: 'MM-12', state_region: 'Kayah', remark: 'Nothing to say' },
    { state_id: '10', state_code:'MM-13', state_region: 'Kayin', remark: 'Nothing to say' },
    { state_id: '11', state_code:'MM-14', state_region: 'Chin', remark: 'Nothing to say' },
    { state_id: '12', state_code:'MM-15', state_region: 'Mon', remark: 'Nothing to say' },
    { state_id: '13', state_code:'MM-16', state_region: 'Rakhine', remark: 'Nothing to say' },
    { state_id: '14', state_code:'MM-17', state_region: 'Shan', remark: 'Nothing to say' },
    { state_id: '15', state_code:'MM-18', state_region: 'Nay Pyi Taw', remark: 'Nothing to say' },

  ]);

  return (
    <MaterialTable
      title="State Preview"
      fontFamily="Poppins"
      columns={columns}
      data={data}

      options={{
        paging:false,
        sorting:false,
        headerStyle:{fontWeight:'bold', fontSize:16, color:'#000', fontFamily:'Poppins'},
        rowStyle:{fontWeight:'normal', fontSize:16, color:'#505050', fontFamily:'Poppins'},
        actionsCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'},
        filterCellStyle:{fontWeight:'bold', fontSize:12, color:'#000'},
        
      }}
      
      editable={{

        onRowAdd: newData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              setData([...data, newData]);
              resolve();
            }, 1000)
          }),

        onRowUpdate: (newData, oldData) =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const dataUpdate = [...data];
              const index = oldData.tableData.id;
              dataUpdate[index] = newData;
              setData([...dataUpdate]);

              resolve();
            }, 1000)
          }),

        onRowDelete: oldData =>
          new Promise((resolve, reject) => {
            setTimeout(() => {
              const dataDelete = [...data];
              const index = oldData.tableData.id;
              dataDelete.splice(index, 1);
              setData([...dataDelete]);
              
              resolve()
            }, 1000)
          }),
      }}
    />
  )
}

export default State_Table;